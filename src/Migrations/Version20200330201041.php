<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200330201041 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE lista_tareas (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE proyecto (id INT AUTO_INCREMENT NOT NULL, coste NUMERIC(10, 0) DEFAULT NULL, fecha_inicio DATE DEFAULT NULL, fecha_fin DATE DEFAULT NULL, presupuesto NUMERIC(10, 0) DEFAULT NULL, nombre VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tarea (id INT AUTO_INCREMENT NOT NULL, usuario_id INT DEFAULT NULL, proyecto_id INT DEFAULT NULL, lista_tareas_id INT DEFAULT NULL, porcentaje NUMERIC(10, 0) DEFAULT NULL, presupuesto NUMERIC(10, 0) DEFAULT NULL, coste NUMERIC(10, 0) DEFAULT NULL, fecha_inicio DATE DEFAULT NULL, fecha_fin DATE DEFAULT NULL, nombre VARCHAR(255) DEFAULT NULL, INDEX IDX_3CA05366DB38439E (usuario_id), INDEX IDX_3CA05366F625D1BA (proyecto_id), INDEX IDX_3CA05366DDBC81CF (lista_tareas_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuario (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON DEFAULT NULL, password VARCHAR(255) NOT NULL, valoracion NUMERIC(10, 0) DEFAULT NULL, nombre VARCHAR(255) DEFAULT NULL, foto_url VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_2265B05DE7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuario_proyecto (usuario_id INT NOT NULL, proyecto_id INT NOT NULL, INDEX IDX_8713F020DB38439E (usuario_id), INDEX IDX_8713F020F625D1BA (proyecto_id), PRIMARY KEY(usuario_id, proyecto_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tarea ADD CONSTRAINT FK_3CA05366DB38439E FOREIGN KEY (usuario_id) REFERENCES usuario (id)');
        $this->addSql('ALTER TABLE tarea ADD CONSTRAINT FK_3CA05366F625D1BA FOREIGN KEY (proyecto_id) REFERENCES proyecto (id)');
        $this->addSql('ALTER TABLE tarea ADD CONSTRAINT FK_3CA05366DDBC81CF FOREIGN KEY (lista_tareas_id) REFERENCES lista_tareas (id)');
        $this->addSql('ALTER TABLE usuario_proyecto ADD CONSTRAINT FK_8713F020DB38439E FOREIGN KEY (usuario_id) REFERENCES usuario (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE usuario_proyecto ADD CONSTRAINT FK_8713F020F625D1BA FOREIGN KEY (proyecto_id) REFERENCES proyecto (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tarea DROP FOREIGN KEY FK_3CA05366DDBC81CF');
        $this->addSql('ALTER TABLE tarea DROP FOREIGN KEY FK_3CA05366F625D1BA');
        $this->addSql('ALTER TABLE usuario_proyecto DROP FOREIGN KEY FK_8713F020F625D1BA');
        $this->addSql('ALTER TABLE tarea DROP FOREIGN KEY FK_3CA05366DB38439E');
        $this->addSql('ALTER TABLE usuario_proyecto DROP FOREIGN KEY FK_8713F020DB38439E');
        $this->addSql('DROP TABLE lista_tareas');
        $this->addSql('DROP TABLE proyecto');
        $this->addSql('DROP TABLE tarea');
        $this->addSql('DROP TABLE usuario');
        $this->addSql('DROP TABLE usuario_proyecto');
    }
}
