<?php

namespace App\Repository;

use App\Entity\Tarea;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Tarea|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tarea|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tarea[]    findAll()
 * @method Tarea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TareaCompletaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tarea::class);
    }


    public function encontrarPorcentaje($percentMin, $percentMax): array
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.porcentaje >= :porcentajeMin')
            ->andWhere('p.porcentaje <= :porcentajeMax')
            ->setParameter('porcentajeMin', $percentMin)
            ->setParameter('porcentajeMax', $percentMax)
            ->getQuery()
            ->getResult();

    }
}
