<?php

namespace App\Repository;

use App\Entity\Proyecto;
use App\Entity\Usuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Proyecto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Proyecto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Proyecto[]    findAll()
 * @method Proyecto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProyectoRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Proyecto::class);
        $this->manager = $manager;
    }

    public function crearProyecto($coste, $presupuesto, $nombre, $fechaInicio, $fechaFin): Proyecto{
        $newProyecto = new Proyecto();
        $newProyecto->setCoste($coste);
        $newProyecto->setPresupuesto($presupuesto);
        $newProyecto->setNombre($nombre);
        $newProyecto->setFechaInicio($fechaInicio);
        $newProyecto->setFechaFin($fechaFin);

        $this->manager->persist($newProyecto);
        $this->manager->flush();
        return $newProyecto;
    }

    public function modificarProyecto(Proyecto $proyecto): Proyecto{
        $this->manager->persist($proyecto);
        $this->manager->flush();

        return $proyecto;
    }

    public function borrarProyecto($proyecto){
        $this->manager->remove($proyecto);
        $this->manager->flush();
    }


}
