<?php

namespace App\Repository;

use App\Entity\ListaTareas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ListaTareas|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListaTareas|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListaTareas[]    findAll()
 * @method ListaTareas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListaTareasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ListaTareas::class);
    }

    // /**
    //  * @return ListaTareas[] Returns an array of ListaTareas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ListaTareas
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
