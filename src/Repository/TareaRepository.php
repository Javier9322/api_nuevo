<?php

namespace App\Repository;

use App\Entity\Tarea;
use App\Entity\Usuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Tarea|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tarea|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tarea[]    findAll()
 * @method Tarea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TareaRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Tarea::class);
        $this->manager = $manager;
    }

    public function crearTarea($coste, $presupuesto, $nombre, $fechaInicio, $fechaFin, $proyecto): Tarea{
        $newTarea = new Tarea();
        $newTarea->setCoste($coste);
        $newTarea->setPresupuesto($presupuesto);
        $newTarea->setNombre($nombre);
        $newTarea->setFechaInicio($fechaInicio);
        $newTarea->setFechaFin($fechaFin);
        $newTarea->setProyecto($proyecto);

        $this->manager->persist($newTarea);
        $this->manager->flush();
        return $newTarea;
    }

    public function modificarTarea(Tarea $tarea): Tarea{
        $this->manager->persist($tarea);
        $this->manager->flush();

        return $tarea;
    }

    public function borrarTarea($tarea){
        $this->manager->remove($tarea);
        $this->manager->flush();
    }


    public function encontrarPorcentaje($percentMin, $percentMax): array
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.porcentaje >= :porcentajeMin')
            ->andWhere('p.porcentaje <= :porcentajeMax')
            ->setParameter('porcentajeMin', $percentMin)
            ->setParameter('porcentajeMax', $percentMax)
            ->getQuery()
            ->getResult();

    }
}