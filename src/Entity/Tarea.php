<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TareaRepository")
 */
class Tarea implements \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="decimal", precision=10, scale=0, nullable=true)
     */
    private $porcentaje;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0, nullable=true)
     */
    private $presupuesto;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0, nullable=true)
     */
    private $coste;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Type("\DateTime")
     */
    private $fechaInicio;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Type("\DateTime")
     * @Assert\GreaterThan(propertyPath="fechaInicio")
     */
    private $fechaFin;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="tareas")
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Proyecto", inversedBy="tareas")
     */
    private $proyecto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ListaTareas", inversedBy="tareas")
     */
    private $listaTareas;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPorcentaje(): ?string
    {
        return $this->porcentaje;
    }

    public function setPorcentaje(?string $porcentaje): self
    {
        $this->porcentaje = $porcentaje;

        return $this;
    }

    public function getPresupuesto(): ?string
    {
        return $this->presupuesto;
    }

    public function setPresupuesto(?string $presupuesto): self
    {
        $this->presupuesto = $presupuesto;

        return $this;
    }

    public function getCoste(): ?string
    {
        return $this->coste;
    }

    public function setCoste(?string $coste): self
    {
        $this->coste = $coste;

        return $this;
    }

    public function getFechaInicio(): ?\DateTimeInterface
    {
        return $this->fechaInicio;
    }

    public function setFechaInicio(?\DateTimeInterface $fechaInicio): self
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    public function getFechaFin(): ?\DateTimeInterface
    {
        return $this->fechaFin;
    }

    public function setFechaFin(?\DateTimeInterface $fechaFin): self
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    public function getUsuario(): ?Usuario
    {
        return $this->usuario;
    }

    public function setUsuario(?Usuario $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getProyecto(): ?Proyecto
    {
        return $this->proyecto;
    }

    public function setProyecto(?Proyecto $proyecto): self
    {
        $this->proyecto = $proyecto;

        return $this;
    }

    public function getListaTareas(): ?ListaTareas
    {
        return $this->listaTareas;
    }

    public function setListaTareas(?ListaTareas $listaTareas): self
    {
        $this->listaTareas = $listaTareas;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->presupuesto,
            $this->coste,
            $this->fechaInicio,
            $this->fechaFin,
            $this->usuario,
            $this->proyecto,
            $this->nombre,
            $this->listaTareas,
        ));
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->presupuesto,
            $this->coste,
            $this->fechaInicio,
            $this->fechaFin,
            $this->usuario,
            $this->proyecto,
            $this->nombre,
            $this->listaTareas,
            )=unserialize($serialized);
    }
}
