<?php

namespace App\Controller;

use App\Entity\ListaTareas;
use App\Entity\Proyecto;
use App\Entity\Tarea;
use App\Entity\Usuario;
use App\Form\ListaTareasType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Controller\AuthController;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;


/**
 * Class TareaController
 * @package App\Controller
 *
 * @Route("/api")
 */
class TareaController extends AbstractController
{

    /**
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     * @Route("/tarea/{id}", name="get_one_tarea", methods={"GET"})
     */
    public function getTarea($id): JsonResponse
    {
        $tarea= $this->getDoctrine()->getRepository(Tarea::class)->find($id);

        if(!$tarea){
            $data=[
                'status' => 404,
                'errors' => "Tarea no encontrada"
            ];
            return $this->response($data,404);
        }else {
            $data = [
                'id' => $tarea->getId(),
                'presupuesto' => $tarea->getPresupuesto(),
                'coste' => $tarea->getCoste(),
                'fechaInicio' => $tarea->getFechaInicio(),
                'fechaFin' => $tarea->getFechaFin()
            ];

            return $this->response($data, 200);
        }
    }
    /**
     * @return JsonResponse
     * @throws \Exception
     * @Route("/tarea", name="get_all_tareas", methods={"GET"})
     */
    public function getTareas(): JsonResponse
    {
        $tareas= $this->getDoctrine()->getRepository(Tarea::class)->findAll();
        $data = [];

        foreach ($tareas as $tarea) {
            $data []= [
                'id' => $tarea->getId(),
                'usuario' => $tarea->getUsuario(),
                'presupuesto' => $tarea->getPresupuesto(),
                'porcentaje' => $tarea->getPorcentaje(),
                'coste' => $tarea->getCoste(),
                'fechaInicio' => $tarea->getFechaInicio(),
                'fechaFin' => $tarea->getFechaFin(),
                'nombre' => $tarea->getNombre()
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);

    }
    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     * @Route("/tarea/{id}", name="put_tarea", methods={"PUT"})
     */
    public function putTarea(Request $request, $id): JsonResponse
    {
        try {
            $tarea = $this->getDoctrine()->getRepository(Tarea::class)->find($id);
            if(!$tarea){
                $data = [
                    'status' => 404,
                    'errors' => "Task not found",
                ];
                return $this->response($data, 404);
            }
            $request = $this->transformJsonBody($request);

            if (!$request ){
                throw new \Exception();
            }

            empty($request->get('usuario')) ? true : $tarea->setUsuario($this->getDoctrine()->getRepository(Usuario::class)->find($request->get('usuario')));
            empty($request->get('coste')) ? true : $tarea->setCoste($request->get('coste'));
            empty($request->get('porcentaje')) ? true : $tarea->setPorcentaje($request->get('porcentaje'));
            empty($request->get('presupuesto')) ? true : $tarea->setPresupuesto($request->get('presupuesto'));
            empty($request->get('nombre')) ? true : $tarea->setNombre($request->get('nombre'));
            empty($request->get('fechaInicio')) ? true : $tarea->setFechaInicio(new \DateTime($request->get('fechaInicio')));
            empty($request->get('fechaFin')) ? true : $tarea->setFechaFin(new \DateTime($request->get('fechaFin')));
            empty($request->get('proyecto')) ? true : $tarea->setProyecto($this->getDoctrine()->getRepository(Proyecto::class)->find($request->get('proyecto')));

            $this->getDoctrine()->getRepository(Tarea::class)->modificarTarea($tarea);

            $data = [
                'status' => 200,
                'errors' => "Task updated successfully",
            ];
            return $this->response($data, 200);
        }catch (\Exception $e){
            $data = [
                'status' => 422,
                'errors' => "Data no valid",
            ];
            return $this->response($data, 422);
        }
    }
    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     * @Route("/tarea", name="add_tarea", methods={"POST"})
     */
    public function addTarea(Request $request): Response
    {
        try {
            $request = $this->transformJsonBody($request);

            if (!$request){
                throw new \Exception();
            }
            $coste = $request->get('coste');
            $presupuesto = $request->request->get('presupuesto');
            $nombre = $request->get('nombre');
            $fechaInicio = new \DateTime($request->get('fechaInicio'));
            $fechaFin = new \DateTime($request->get('fechaFin'));
            $proyecto = $this->getDoctrine()->getRepository(Proyecto::class)->find($request->get('proyecto'));

            $nuevaTarea = $this->getDoctrine()->getRepository(Tarea::class)->crearTarea($coste, $presupuesto, $nombre, $fechaInicio, $fechaFin, $proyecto);
            // Hacer el serializer de nuevaTarea para mostrar los campos
            //return $this->response($nuevaTarea->getId(), 200);

            $defaultContext = [
                    AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                        return $object;
                    },
                ];
            $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

             $encoder= new JsonEncoder();

            $serializer = new Serializer(array($normalizer), array($encoder));

            $jsonContent =  $serializer->serialize($nuevaTarea, "json");
            $response = new Response();
            $response ->headers->set("Content-Type", "application/json");
            $response->setContent($jsonContent);

        return $response;


        }catch(\Exception $e){
            $data = [
                'status' => 422,
                'errors' => "Datos invalidos",
            ];
            return $this->response($data, 422);
        }


    }
    /**
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     * @Route("/tarea/{id}", name="borrar_tarea", methods={"DELETE"})
     */
    public function deleteTarea($id)
    {
        $tarea= $this->getDoctrine()->getRepository(Tarea::class)->find($id);

        if(!$tarea){
            $data = [
                'status' => 404,
                'errors' => "Task not found",
            ];
            return $this->response($data, 404);
        }

        $this->getDoctrine()->getRepository(Tarea::class)->borrarTarea($tarea);

        $data = [
            'status' => 200,
            'errors' => "Tarea borrada",
        ];
        return $this->response($data, 200);

    }

    /**
     * Returns a JSON response
     *
     * @param array $data
     * @param $status
     * @param array $headers
     * @return JsonResponse
     */
    public function response($data, $status = 200, $headers = [])
    {
        return new JsonResponse($data, $status, $headers);
    }

    protected function transformJsonBody(\Symfony\Component\HttpFoundation\Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return $request;
        }

        $request->request->replace($data);

        return $request;
    }

}