<?php

namespace App\Controller;

use App\Entity\Proyecto;
use App\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TareaController
 * @package App\Controller
 *
 * @Route("/api")
 */
class UsuarioController extends AbstractController
{
    /**
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     * @Route("/usuario", name="get_one_user", methods={"GET"})
     */
    public function getUsuario(): JsonResponse
    {
        $usuario= $this->getUser();

        if(!$usuario){
            $data=[
                'status' => 404,
                'errors' => "Usuario no encontrado"
            ];
            return $this->response($data,404);
        }else {
            $data = [
                'id' => $usuario->getId(),
                'email' => $usuario->getEmail(),
                'nombre' => $usuario->getNombre(),
                'roles' => $usuario->getRoles(),
                'fotoUrl' => $usuario->getFotoUrl()
            ];

            return $this->response($data, 200);
        }
    }
    /**
     * @return JsonResponse
     * @throws \Exception
     * @Route("/usuarios", name="get_all_users", methods={"GET"})
     */
    public function getUsuarios(): JsonResponse
    {
        $usuarios= $this->getDoctrine()->getRepository(Usuario::class)->findAll();
        $data = [];

        foreach ($usuarios as $usuario) {
            $data []= [
                'id' => $usuario->getId(),
                'email' => $usuario->getEmail(),
                'nombre' => $usuario->getNombre(),
                'roles' => $usuario->getRoles(),
                'fotoUrl' => $usuario->getFotoUrl()
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);

    }
    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     * @Route("/usuario/{id}", name="put_usuario", methods={"PUT"})
     */
    public function putUsuario(Request $request, $id): JsonResponse
    {
        try {
            $usuario = $this->getDoctrine()->getRepository(Usuario::class)->find($id);
            if(!$usuario){
                $data = [
                    'status' => 404,
                    'errors' => "User not found",
                ];
                return $this->response($data, 404);
            }
            $request = $this->transformJsonBody($request);

            if (!$request ){
                throw new \Exception();
            }

            empty($request->get('email')) ? true : $usuario->setEmail($request->get('email'));
            empty($request->get('nombre')) ? true : $usuario->setNombre($request->get('nombre'));

            $this->getDoctrine()->getRepository(Usuario::class)->modificarUsuario($usuario);

            $data = [
                'status' => 200,
                'errors' => "User updated successfully",
            ];
            return $this->response($data, 200);


        }catch (\Exception $e){
            $data = [
                'status' => 422,
                'errors' => "Data no valid",
            ];
            return $this->response($data, 422);
        }
    }

    /**
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     * @Route("/usuario/{id}", name="borrar_usuario", methods={"DELETE"})
     */
    public function deleteUsuario($id)
    {
        $usuario= $this->getDoctrine()->getRepository(Usuario::class)->find($id);

        if(!$usuario){
            $data = [
                'status' => 404,
                'errors' => "User not found",
            ];
            return $this->response($data, 404);
        }

        $this->getDoctrine()->getRepository(Usuario::class)->borrarUsuario($usuario);

        $data = [
            'status' => 200,
            'errors' => "User deleted",
        ];
        return $this->response($data, 200);

    }
    /**
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     * @Route("/usuario/tareas", name="usuario_id_tareas", methods={"GET"})
    */
    public function getTareasUsuario(){
        $usuario= $this->getUser();
        $tareasUsuario= $usuario->getTareas();
        $data= [];
        if($tareasUsuario->isEmpty()){
            return $this->response($data,200);
        }
        foreach ($tareasUsuario as $tarea) {
            $data []= [
                'id' => $tarea->getId(),
                'usuario' => $tarea->getUsuario(),
                'presupuesto' => $tarea->getPresupuesto(),
                'porcentaje' => $tarea->getPorcentaje(),
                'coste' => $tarea->getCoste(),
                'fechaInicio' => $tarea->getFechaInicio(),
                'fechaFin' => $tarea->getFechaFin(),
                'nombre' => $tarea->getNombre()
            ];

        }
        return new JsonResponse($data, Response::HTTP_OK);

    }


    /**
     * Returns a JSON response
     *
     * @param array $data
     * @param $status
     * @param array $headers
     * @return JsonResponse
     */
    public function response($data, $status = 200, $headers = [])
    {
        return new JsonResponse($data, $status, $headers);
    }

    protected function transformJsonBody(\Symfony\Component\HttpFoundation\Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return $request;
        }

        $request->request->replace($data);

        return $request;
    }
}
