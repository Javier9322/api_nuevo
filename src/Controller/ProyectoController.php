<?php

namespace App\Controller;

use App\Entity\Proyecto;
use App\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class TareaController
 * @package App\Controller
 *
 * @Route("/api")
 */
class ProyectoController extends AbstractController
{
    /**
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     * @Route("/proyecto/{id}", name="get_one_proyect", methods={"GET"})
     */
    public function getProyecto($id): JsonResponse
    {
        $proyecto= $this->getDoctrine()->getRepository(Proyecto::class)->find($id);

        if(!$proyecto){
            $data=[
                'status' => 404,
                'errors' => "Proyecto no encontrado"
            ];
            return $this->response($data,404);
        }else {
            $data = [
                'id' => $proyecto->getId(),
                'presupuesto' => $proyecto->getPresupuesto(),
                'coste' => $proyecto->getCoste(),
                'fechaInicio' => $proyecto->getFechaInicio(),
                'fechaFin' => $proyecto->getFechaFin()
            ];

            return $this->response($data, 200);
        }
    }
    /**
     * @return JsonResponse
     * @throws \Exception
     * @Route("/proyecto", name="get_all_proyectos", methods={"GET"})
     */
    public function getProyectos(): JsonResponse
    {
        $proyectos= $this->getDoctrine()->getRepository(Proyecto::class)->findAll();
        $data = [];

        foreach ($proyectos as $proyecto) {
            $data []= [
                'id' => $proyecto->getId(),
                'usuarios' => $proyecto->getUsuarios(),
                'presupuesto' => $proyecto->getPresupuesto(),
                'coste' => $proyecto->getCoste(),
                'fechaInicio' => $proyecto->getFechaInicio(),
                'fechaFin' => $proyecto->getFechaFin(),
                'nombre'=> $proyecto->getNombre()
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);

    }
    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     * @Route("/proyecto/{id}", name="put_proyecto", methods={"PUT"})
     */
    public function putProyecto(Request $request, $id): JsonResponse
    {
        try {
            $proyecto = $this->getDoctrine()->getRepository(Proyecto::class)->find($id);
            if(!$proyecto){
                $data = [
                    'status' => 404,
                    'errors' => "Proyect not found",
                ];
                return $this->response($data, 404);
            }
            $request = $this->transformJsonBody($request);

            if (!$request ){
                throw new \Exception();
            }

            empty($request->get('coste')) ? true : $proyecto->setCoste($request->get('coste'));
            empty($request->get('presupuesto')) ? true : $proyecto->setPresupuesto($request->get('presupuesto'));
            empty($request->get('fechaInicio')) ? true : $proyecto->setFechaInicio(new \DateTime($request->get('fechaInicio')));
            empty($request->get('fechaFin')) ? true : $proyecto->setFechaFin(new \DateTime($request->get('fechaFin')));
            empty($request->get('usuario')) ? true : $proyecto->addUsuario($this->getDoctrine()->getRepository(Usuario::class)->find($request->get('usuario')));

            $this->getDoctrine()->getRepository(Proyecto::class)->modificarProyecto($proyecto);

            $data = [
                'status' => 200,
                'errors' => "Proyect updated successfully",
            ];
            return $this->response($data, 200);


        }catch (\Exception $e){
            $data = [
                'status' => 422,
                'errors' => "Data no valid",
            ];
            return $this->response($data, 422);
        }
    }
    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     * @Route("/proyecto", name="add_proyecto", methods={"POST"})
     */
    public function addProyecto(Request $request): Response
    {
        try {
            $request = $this->transformJsonBody($request);

            if (!$request){
                throw new \Exception();
            }
            $coste = $request->get('coste');
            $presupuesto = $request->request->get('presupuesto');
            $nombre = $request->get('nombre');
            $fechaInicio = new \DateTime($request->get('fechaInicio'));
            $fechaFin= new \DateTime($request->get('fechaFin'));

            $datos = [
                'status' => 200,
                'success' => "Proyect added successfully",
            ];
            $nuevoProyecto = $this->getDoctrine()->getRepository(Proyecto::class)->crearProyecto($coste, $presupuesto, $nombre, $fechaInicio, $fechaFin);

            $defaultContext = [
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                    return $object;
                },
            ];
            $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

            $encoder= new JsonEncoder();

            $serializer = new Serializer(array($normalizer), array($encoder));

            $jsonContent =  $serializer->serialize($nuevoProyecto, "json");
            $response = new Response();
            $response ->headers->set("Content-Type", "application/json");
            $response->setContent($jsonContent);

            return $response;

        }catch(\Exception $e){
            $data = [
                'status' => 422,
                'errors' => "Datos invalidos",
            ];
            return $this->response($data, 422);
        }
    }
    /**
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     * @Route("/proyecto/{id}", name="borrar_proyecto", methods={"DELETE"})
     */
    public function deleteProyecto($id)
    {
        $proyecto= $this->getDoctrine()->getRepository(Proyecto::class)->find($id);

        if(!$proyecto){
            $data = [
                'status' => 404,
                'errors' => "Proyect not found",
            ];
            return $this->response($data, 404);
        }

        $this->getDoctrine()->getRepository(Proyecto::class)->borrarProyecto($proyecto);

        $data = [
            'status' => 200,
            'errors' => "Proyect deleted",
        ];
        return $this->response($data, 200);

    }

    /**
     * @param $id
     * @return Response
     * @throws \Exception
     * @Route("/proyectos/tareas", name="proyecto_id_tareas", methods={"GET"})
     */
    public function getTareasProyecto()
    {
        //

        $usuario = $this->getUser();
        //Falta filtrar por rol JEFE
        $proyectosUser = $usuario->getProyectos();
        //$proyectos= $this->getDoctrine()->getRepository(Proyecto::class)->findAll();
       // $data = [];
        $data = $proyectosUser;
        /*foreach ($proyectosUser as $proyecto) {
            $tareasProyecto = $proyecto->getTareas();
            $data[$proyecto->getId()]=$tareasProyecto;
        }*/
        //return new JsonResponse($data, Response::HTTP_OK);

        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object;
            },
        ];
        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

        $encoder= new JsonEncoder();

        $serializer = new Serializer(array($normalizer), array($encoder));

        $jsonContent =  $serializer->serialize($data, "json");
        $response = new Response();
        $response ->headers->set("Content-Type", "application/json");
        $response->setContent($jsonContent);

        return $response;

    }

    /**
     * Returns a JSON response
     *
     * @param array $data
     * @param $status
     * @param array $headers
     * @return JsonResponse
     */
    public function response($data, $status = 200, $headers = [])
    {
        return new JsonResponse($data, $status, $headers);
    }

    protected function transformJsonBody(\Symfony\Component\HttpFoundation\Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return $request;
        }

        $request->request->replace($data);

        return $request;
    }
}
