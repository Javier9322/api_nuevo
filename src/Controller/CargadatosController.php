<?php

namespace App\Controller;

use App\Entity\Proyecto;
use App\Entity\Usuario;
use App\Entity\Tarea;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CargadatosController extends AbstractController
{
    /**
     * @Route("/cargadatos", name="cargardatos")
     */
    public function cargarDatos(){
        $entityManager = $this -> getDoctrine() -> getManager();

        $proyecto1 = new Proyecto();
        $proyecto2 = new Proyecto();

        $tarea1 = new Tarea();
        $tarea2 = new Tarea();
        $tarea3 = new Tarea();
        $tarea4 = new Tarea();

        $usuario1 = new Usuario();
        $usuario2 = new Usuario();
        $usuario3 = new Usuario();

        $tarea1->setCoste("1000");
        $tarea2->setCoste("1000");
        $tarea3->setCoste("1000");
        $tarea4->setCoste("1000");

        $tarea1->setPresupuesto("1500");
        $tarea2->setPresupuesto("1500");
        $tarea3->setPresupuesto("1500");
        $tarea4->setPresupuesto("1500");

        $tarea1->setPorcentaje("0");
        $tarea2->setPorcentaje("0");
        $tarea3->setPorcentaje("0");
        $tarea4->setPorcentaje("0");

        $tarea1->setNombre("t1");
        $tarea2->setNombre("t2");
        $tarea3->setNombre("t3");
        $tarea4->setNombre("t4");

        $tarea1->setFechaInicio(new \DateTime("2020-01-10"));
        $tarea2->setFechaInicio(new \DateTime("2020-02-05"));
        $tarea3->setFechaInicio(new \DateTime("2020-01-20"));
        $tarea4->setFechaInicio(new \DateTime("2020-01-30"));

        $tarea1->setFechaFin(new \DateTime("2020-01-10"));
        $tarea2->setFechaFin(new \DateTime("2020-02-05"));
        $tarea3->setFechaFin(new \DateTime("2020-01-20"));
        $tarea4->setFechaFin(new \DateTime("2020-01-30"));


        $usuario1->setEmail("jefe@gmail.com");
        $usuario2->setEmail("admin@gmail.com");
        $usuario3->setEmail("user1@gmail.com");

        $usuario1->setFotoUrl("fotoUser1.jpg");
        $usuario2->setFotoUrl("fotoUser2.jpg");
        $usuario3->setFotoUrl("fotoUser3.jpg");

        $usuario1->setNombre("user1");
        $usuario2->setNombre("user2");
        $usuario3->setNombre("user3");

        $usuario1->setPassword('$argon2id$v=19$m=65536,t=4,p=1$WUExWVc3bERxaVBCYmZ0bA$hXMfk+ViNGzu50xd0C05GXMYI7a2GW30FQ85tvGfHPQ');
        $usuario2->setPassword('$argon2id$v=19$m=65536,t=4,p=1$a2JCMG0xT2N5ZWtJYUlSZw$0P1/HNueHngBv/firJJZZ66AgH8EoKVox+APIwP1L9Q');
        $usuario3->setPassword('$argon2id$v=19$m=65536,t=4,p=1$bjN1Q2hWM2l2dWNqYVhBZQ$5bbxom7VRxxCRwt6WmmknXqdSMfCyhsuv4iDsvQDz4s');
        $usuario1->addProyecto($proyecto1);
        $usuario2->addProyecto($proyecto1);
        $usuario3->addProyecto($proyecto1);

        $usuario1->addTarea($tarea4);
        $usuario2->addTarea($tarea3);
        $usuario3->addTarea($tarea2);
        $usuario1->addTarea($tarea1);
        $usuario1->setRoles(['ROLE_JEFE']);
        $usuario2->setRoles(['ROLE_ADMIN']);
        $usuario3->setRoles(['ROLE_USER']);


        $proyecto1->addTarea($tarea1);
        $proyecto1->addTarea($tarea2);
        $proyecto1->addTarea($tarea3);
        $proyecto2->addTarea($tarea4);

        $proyecto1->addUsuario($usuario1);
        $proyecto1->addUsuario($usuario2);
        $proyecto1->addUsuario($usuario3);
        $proyecto2->addUsuario($usuario1);


        $proyecto1->setPresupuesto("10000");
        $proyecto1->setCoste("5000");
        $proyecto1->setNombre("Proyecto Drones");


        $proyecto2->setPresupuesto("10000");
        $proyecto2->setCoste("5000");
        $proyecto2->setNombre("Desarrollo web");


        $entityManager->persist($usuario1);
        $entityManager->persist($usuario2);
        $entityManager->persist($usuario3);

        $entityManager->persist($tarea1);
        $entityManager->persist($tarea2);
        $entityManager->persist($tarea3);
        $entityManager->persist($tarea4);
        $entityManager->persist($proyecto1);
        $entityManager->persist($proyecto2);

        $entityManager->flush();

        return new Response ('Datos cargados OK');
    }

}
