var token=null;
var urlApi = "http://api_gestor_de_tareas.local/api";
$(document).ready(function() {
    $("#logoutBoton").hide();
    $("#divTareas").hide();
    $("#divProyectos").hide();
    if(token==null){
        loginUsuario();

    }
});

function inicia(usuario, password) {

    $.ajax(
        {
            url: urlApi+"/login_check",
            type: "POST",
            async: true,
            contentType: 'application/json',
            data: JSON.stringify({username: usuario, password: password}),
            dataType: "json",
            success: function (dataToken) {
                //console.log(dataToken.token);
                $("#logoutBoton").show();
                $("#divTareas").show();
                $("#divProyectos").show();
                $("#imagen").hide();
                token= dataToken.token;
                cargaTareasUsuario();
                cargaGestionTareas();
                $("#logoutBoton").click(function () {
                    token=null;
                    var url = document.getElementById("logoutBoton");
                    url.setAttribute("href", "http://api_gestor_de_tareas.local/frontend/html/PaginaPrincipal.html");
                })

            },
            error:function () {
                var divLogin = document.getElementById("divLogin");
                var pError= document.createElement("p");
                var textoError = document.createTextNode("Usuario o contraseña incorrectos");

                pError.appendChild(textoError);
                divLogin.appendChild(pError);

            }

        });
        // Tambien se puede hacer con .fail
        /*.fail(function () {
            // Handle error here
                alert("sin autorizacion");
        });*/
}

function cargaTareasUsuario() {
    $.ajax(
        {
            url: urlApi+"/usuario/tareas",
            type: "GET",
            async: true,
            data: {},
            headers :{'Authorization': 'Bearer '+token},
            dataType: "json",
            success: function (listaTareas) {
                $("#editable tbody").html("");
                //console.log(listaTareas.length);
                if (listaTareas.length <= 0) {
                } else {

                    $.each(listaTareas, function () {
                        //console.log(this.fechaInicio);
                        var fechaInicio = new Date(this.fechaInicio.date);
                        var sfechaInicio = fechaInicio.toISOString().substring(0, 10);
                        var fechaFin = new Date(this.fechaFin.date);
                        var sfechaFin = fechaFin.toISOString().substring(0, 10);
                        var fila = "<tr id='filaTarea" + this.id + "'><td class='uneditable id'>" + this.id + "</td><td class='nombre' >" + this.nombre + "</td><td class='fechaInicio' ><input type='date' id='fechaInicio" + this.id + "' value='" + sfechaInicio + "'></td><td class='fechaFin' ><input type='date' id='fechaFin" + this.id + "' value='" + sfechaFin + "'></td><td class='presupuesto' >" + this.presupuesto + "</td><td class='coste'>" + this.coste + "</td><td class='porcentaje'>" + this.porcentaje + "</td>\n\
                                                            </tr>";
                        $("#editable").append(fila);
                    });
                    $('#editable').editableTableWidget();
                    $('#editable td.uneditable').on('change', function (evt, newValue) {
                        return false;
                    });

                    $('#editable td').on('change', function (evt, newValue) {
                        // console.log($(this).parent().find(".id").html());
                        var id = $(this).parent().find(".id").html();
                        var nombre = $(this).parent().find(".nombre").html();
                        var fechaInicio = document.getElementById('fechaInicio' + id).value;
                        var fechaFin = document.getElementById('fechaFin' + id).value;
                        var presupuesto = $(this).parent().find(".presupuesto").html();
                        var coste = $(this).parent().find(".coste").html();
                        var porcentaje = $(this).parent().find(".porcentaje").html();

                        //Problemas con las fechas

                        if (confirm("¿Seguro que deseas actualizar?")) {
                            $.ajax(
                                {
                                    url: urlApi+"/tarea/" + id,
                                    type: "PUT",
                                    async: true,
                                    data: {
                                        "coste": coste,
                                        "porcentaje": porcentaje,
                                        "presupuesto": presupuesto,
                                        "nombre": nombre,
                                        "fechaInicio": fechaInicio,
                                        "fechaFin": fechaFin
                                    },
                                    headers: {'Authorization': 'Bearer ' + token},
                                    dataType: "json",
                                    success: function (datos) {
                                        //console.log(datos);
                                        if (datos.status == 200) {
                                            document.getElementById('exito').innerHTML = "Tarea " + id + " actualizada con exito";
                                        }
                                    }
                                }
                            );
                        } else {
                            document.getElementById('exito').innerHTML = "No has actualizado las tareas";
                        }
                    });
                }
            }
        }
    );
}


function cargaGestionTareas() {
    $.ajax(

        {
            url: urlApi+"/usuario",
            type: "GET",
            async: true,
            data: {},
            headers :{'Authorization': 'Bearer '+token},
            dataType: "json",
            success: function (usuario) {

                var roles = usuario.roles;
                document.getElementById("fotoUsuario").setAttribute("src", usuario.fotoUrl);

                var divLogin= document.getElementById("divLogin");
                var divHtml = document.createElement("divDatosUser");

                var campoEmail = document.createElement("p");
                var campoNombre = document.createElement("p");
                var emailUser = document.createTextNode(usuario.email);
                var nombreUser = document.createTextNode(usuario.nombre);
                campoEmail.appendChild(emailUser);
                campoNombre.appendChild(nombreUser);

                divHtml.appendChild(campoEmail);
                divHtml.appendChild(campoNombre);
                divLogin.replaceWith(divHtml);
                //console.log(roles);
                for(var i in roles) {
                    //console.log(roles[i]);
                    if(roles[i]=="ROLE_ADMIN"){
                        cargarUsuariosRoles();
                        $.ajax({
                            url: urlApi + "/proyecto",
                            type: "GET",
                            async: true,
                            data: {},
                            headers: {'Authorization': 'Bearer ' + token},
                            dataType: "json",
                            success: function (proyectos) {

                                //console.log(proyectos);
                                var tablaProyecto = document.createElement('table');
                                var tblHead = document.createElement("thead");

                                var trCabe = document.createElement('tr');
                                var thIdCabe = document.createElement('th');
                                var thNombreCabe = document.createElement('th');
                                var thFechaInicioCabe = document.createElement('th');
                                var thFechaFinCabe = document.createElement('th');
                                var thCosteCabe = document.createElement('th');
                                var thPresupuestoCabe = document.createElement('th');
                                var thUsuarioCabe = document.createElement('th');
                                var thRemove = document.createElement('th');

                                var textoIdCabe = document.createTextNode( 'Id');
                                var textoNombreCabe = document.createTextNode( 'Nombre');
                                var textoFechaInicioCabe = document.createTextNode( 'Fecha de inicio');
                                var textoFechaFinCabe = document.createTextNode( 'Fecha de fin');
                                var textoCosteCabe = document.createTextNode( 'Coste');
                                var textoPresuCabe = document.createTextNode( 'Presupuesto');
                                var textoUsuarioCabe = document.createTextNode( 'Usuario');
                                var textoRemove = document.createTextNode('Borrar');


                                thIdCabe.appendChild(textoIdCabe);
                                thNombreCabe.appendChild(textoNombreCabe);
                                thFechaInicioCabe.appendChild(textoFechaInicioCabe);
                                thFechaFinCabe.appendChild(textoFechaFinCabe);
                                thCosteCabe.appendChild(textoCosteCabe);
                                thPresupuestoCabe.appendChild(textoPresuCabe);
                                thUsuarioCabe.appendChild(textoUsuarioCabe);
                                thRemove.appendChild(textoRemove);


                                trCabe.appendChild(thIdCabe);
                                trCabe.appendChild(thNombreCabe);
                                trCabe.appendChild(thFechaInicioCabe);
                                trCabe.appendChild(thFechaFinCabe);
                                trCabe.appendChild(thCosteCabe);
                                trCabe.appendChild(thPresupuestoCabe);
                                trCabe.appendChild(thUsuarioCabe);
                                trCabe.appendChild(thRemove);

                                tblHead.appendChild(trCabe);
                                tablaProyecto.appendChild(tblHead);

                                var tblBody = document.createElement("tbody");
                                tablaProyecto.appendChild(tblBody);

                                //Usuarios registrados
                                var tablaUsuarioRegistrado = document.createElement('table');
                                var tblHeadRegistrado = document.createElement("thead");

                                var trCabeRegistrado = document.createElement('tr');
                                var thIdCabeRegistrado = document.createElement('th');
                                var thNombreCabeRegistrado = document.createElement('th');
                                var thEmailCabeRegistrado = document.createElement('th');
                                var thUsuarioCabeRegistrado = document.createElement('th');
                                var thRolCabeRegistrado = document.createElement('th');
                                var thRemoveRegistrado = document.createElement('th');
                                var thPassCabeRegistrado = document.createElement('th');

                                var textoIdCabeRegistrado = document.createTextNode( 'Id');
                                var textoNombreCabeRegistrado = document.createTextNode( 'Nombre');
                                var textoEmailCabeRegistrado = document.createTextNode( 'Email');
                                var textoUsuarioCabeRegistrado = document.createTextNode( 'Usuario');
                                var textoRolCabeRegistrado = document.createTextNode( 'Rol');
                                var textoRemoveRegistrado = document.createTextNode('Borrar');
                                var textoPassRegistrado = document.createTextNode('Contraseña');


                                thIdCabeRegistrado.appendChild(textoIdCabeRegistrado);
                                thNombreCabeRegistrado.appendChild(textoNombreCabeRegistrado);
                                thEmailCabeRegistrado.appendChild(textoEmailCabeRegistrado);
                                thRolCabeRegistrado.appendChild(textoRolCabeRegistrado);
                                thUsuarioCabeRegistrado.appendChild(textoUsuarioCabeRegistrado);
                                thRemoveRegistrado.appendChild(textoRemoveRegistrado);
                                thPassCabeRegistrado.appendChild(textoPassRegistrado);


                                trCabeRegistrado.appendChild(thIdCabeRegistrado);
                                trCabeRegistrado.appendChild(thNombreCabeRegistrado);
                                trCabeRegistrado.appendChild(thEmailCabeRegistrado);
                                trCabeRegistrado.appendChild(thPassCabeRegistrado);
                                trCabeRegistrado.appendChild(thRolCabeRegistrado);
                                trCabeRegistrado.appendChild(thUsuarioCabeRegistrado);
                                trCabeRegistrado.appendChild(thRemoveRegistrado);

                                tblHeadRegistrado.appendChild(trCabeRegistrado);
                                tablaUsuarioRegistrado.appendChild(tblHeadRegistrado);
                                var tblBodyRegistrado = document.createElement("tbody");
                                $.ajax({
                                    url: urlApi+"/usuarios",
                                    type: "GET",
                                    async: true,
                                    data: {},
                                    headers :{'Authorization': 'Bearer '+token},
                                    dataType: "json",
                                    success: function (usuarios) {
                                        $.each(usuarios, function () {
                                            console.log(this.fotoUrl);
                                            var trUserRegi = document.createElement("tr");

                                            var tdIdUserRegi = document.createElement("td");
                                            var tdEmailUserRegi = document.createElement("td");
                                            var tdNombreUserRegi = document.createElement("td");
                                            var tdRolUserRegi = document.createElement("td");
                                            var tdFotoUserRegi = document.createElement("td");
                                            var tdPassUserRegi = document.createElement("td");
                                            var tdBoton = document.createElement("td");

                                            var botonBorrar = document.createElement("button");
                                            var textoRemove = document.createTextNode("Borrar");
                                            botonBorrar.setAttribute("id", "borrarBotonUser_"+this.id);
                                            botonBorrar.setAttribute("type", "button");
                                            botonBorrar.setAttribute("value", "Borrar");

                                            botonBorrar.appendChild(textoRemove);
                                            botonBorrar.className="btn btn-danger btn-rounded btn-sm my-0 botonBorrar";

                                            tdBoton.appendChild(botonBorrar);

                                            var textoIdUserRegi = document.createTextNode(this.id);
                                            var textoEmailUserRegi = document.createTextNode(this.email);
                                            var textoNombreUserRegi = document.createTextNode(this.nombre);
                                            var textoRolUserRegi = document.createTextNode(this.roles);
                                            var textoFotoUserRegi = document.createElement("IMG");
                                            var textoPassUserRegi = document.createTextNode("xxx");

                                            textoFotoUserRegi.setAttribute("src", this.fotoUrl);
                                            textoFotoUserRegi.setAttribute("width", "30px");
                                            textoFotoUserRegi.setAttribute("heigth", "30px");

                                            tdIdUserRegi.appendChild(textoIdUserRegi);
                                            tdEmailUserRegi.appendChild(textoEmailUserRegi);
                                            tdNombreUserRegi.appendChild(textoNombreUserRegi);
                                            tdRolUserRegi.appendChild(textoRolUserRegi);
                                            tdFotoUserRegi.appendChild(textoFotoUserRegi);
                                            tdPassUserRegi.appendChild(textoPassUserRegi);

                                            trUserRegi.appendChild(tdIdUserRegi);
                                            trUserRegi.appendChild(tdNombreUserRegi);
                                            trUserRegi.appendChild(tdEmailUserRegi);
                                            trUserRegi.appendChild(tdPassUserRegi);
                                            trUserRegi.appendChild(tdRolUserRegi);
                                            trUserRegi.appendChild(tdFotoUserRegi);
                                            trUserRegi.appendChild(tdBoton);


                                            tblBodyRegistrado.appendChild(trUserRegi);
                                        });
                                    }
                                });

                                tablaUsuarioRegistrado.appendChild(tblBodyRegistrado);
                                tablaUsuarioRegistrado.className = "pure-table pure-table-bordered table-responsive-md text-center";

                                document.getElementById('divUsuariosRegistrados').appendChild(tablaUsuarioRegistrado);


                                $.each(proyectos, function () {
                                    //console.log(this);
                                    var fechaInicio = new Date(this.fechaInicio.date);
                                    var sfechaInicio = fechaInicio.toISOString().substring(0, 10);
                                    var fechaFin = new Date(this.fechaFin.date);
                                    var sfechaFin = fechaFin.toISOString().substring(0, 10);

                                    var tr = document.createElement('tr');
                                    tr.setAttribute("id", "fila_"+this.id);
                                    var tdId = document.createElement('td');
                                    var tdNombre = document.createElement('td');
                                    var tdFechaInicio = document.createElement('td');
                                    var tdFechaFin = document.createElement('td');
                                    var tdCoste = document.createElement('td');
                                    var tdPresupuesto = document.createElement('td');
                                    var tdUsuario = document.createElement('td');
                                    //tdUsuario.setAttribute("ondrop","drop(event)");
                                    //tdUsuario.setAttribute("ondragover","allowDrop(event)");
                                    var tdRemove = document.createElement('td');

                                    var textoId = document.createTextNode( this.id);
                                    var textoNombre = document.createTextNode( this.nombre);
                                    var inputFechaInicio = document.createElement( "input");
                                    inputFechaInicio.setAttribute("type", "date");
                                    inputFechaInicio.setAttribute("value", sfechaInicio);
                                    var inputFechaFin = document.createElement( "input");
                                    inputFechaFin.setAttribute("type", "date");
                                    inputFechaFin.setAttribute("value",sfechaFin);
                                    var textoCoste = document.createTextNode( this.coste);
                                    var textoPresu = document.createTextNode( this.presupuesto);

                                    var botonRemove = document.createElement("input");
                                    var textoRemove = document.createTextNode("Borrar");
                                    botonRemove.setAttribute("id", "borrarBoton_"+this.id);
                                    botonRemove.setAttribute("type", "button");
                                    botonRemove.setAttribute("value", "Borrar");

                                    botonRemove.appendChild(textoRemove);
                                    botonRemove.className="btn btn-danger btn-rounded btn-sm my-0 botonBorrar";
                                    var textoUsuario = document.createElement("IMG");
                                    console.log(this.usuarios);
                                    if(this.usuario==null){
                                        textoUsuario.setAttribute("src", "../../images/interrogacion.jpg");
                                        textoUsuario.setAttribute("width", "30");
                                        textoUsuario.setAttribute("ondrop", "dropProyecto(event)");
                                        textoUsuario.setAttribute("ondragover", "allowDrop(event)");

                                        textoUsuario.setAttribute("height", "30");
                                        textoUsuario.setAttribute("alt", "Foto user");
                                    }else {
                                        textoUsuario.setAttribute("src", this.usuario.fotoUrl);
                                        textoUsuario.setAttribute("width", "30");
                                        textoUsuario.setAttribute("ondrop", "dropProyecto(event)");
                                        textoUsuario.setAttribute("ondragover", "allowDrop(event)");

                                        textoUsuario.setAttribute("height", "30");
                                        textoUsuario.setAttribute("alt", "Foto user");
                                    }

                                    tdId.appendChild(textoId);
                                    tdNombre.appendChild(textoNombre);
                                    tdFechaInicio.appendChild(inputFechaInicio);
                                    tdFechaFin.appendChild(inputFechaFin);
                                    tdCoste.appendChild(textoCoste);
                                    tdPresupuesto.appendChild(textoPresu);
                                    tdUsuario.appendChild(textoUsuario);
                                    tdRemove.appendChild(botonRemove);

                                    tr.appendChild(tdId);
                                    tr.appendChild(tdNombre);
                                    tr.appendChild(tdFechaInicio);
                                    tr.appendChild(tdFechaFin);
                                    tr.appendChild(tdCoste);
                                    tr.appendChild(tdPresupuesto);
                                    tr.appendChild(tdUsuario);
                                    tr.appendChild(tdRemove);

                                    tblBody.appendChild(tr);

                                });

                                tablaProyecto.appendChild(tblBody);
                                tablaProyecto.setAttribute("id", "tabla"+this.id);
                                tablaProyecto.setAttribute("style", "margin-left: 1vw;");

                                tablaProyecto.className = "pure-table pure-table-bordered table-responsive-md text-center";

                                document.getElementById('divTareasProyecto').appendChild(tablaProyecto);


                                tablaUsuarios.appendChild(tblBodyUser);

                                tablaUsuarios.appendChild(tblBodyUser);
                                tablaUsuarios.setAttribute("id", "tablaUser"+this.id);
                                tablaUsuarios.setAttribute("style", "margin-left: 1vw;");

                                tablaUsuarios.className = "pure-table pure-table-bordered table-responsive-md text-center";

                                document.getElementById('divUsuarios').appendChild(tablaUsuarios);
                                $("#divUsuarios").append('<button style="margin-top: 1vw" class="w3-button w3-large w3-circle w3-grey" id="añadeUsuario'+this.id+'">+</button>');
                                $("#añadeUsuario"+this.id).click(function(){
                                    var usuarioNombre = document.getElementById("inputNombre").value;
                                    var passwordUser = document.getElementById("inputPass").value;
                                    var combo = document.getElementById("inputSelect").value;
                                    $.ajax(
                                        {
                                            url: "/register",
                                            type: "POST",
                                            async: true,
                                            data: {
                                                "email":usuarioNombre,
                                                "password": passwordUser,
                                                "roles":combo
                                            },
                                            headers: {'Authorization': 'Bearer ' + token},
                                            dataType: "json",
                                            success: function (datos){

                                            }
                                        });
                                });
                                $('#tabla'+this.id).editableTableWidget();
                                $('#divTareasProyecto').append('<button style="margin-top: 1vw" class="w3-button w3-large w3-circle w3-grey" id="añadeProyecto'+this.id+'">+</button>');

                                $("#añadeProyecto"+this.id).click(function () {
                                    if (confirm("¿Seguro que desea crear un proyecto?")) {
                                        // Hacer ajax post Tarea. On succes insertar nueva fila en tabla con id de tarea que devuelve ajax
                                        // con id de tarea en fila
                                        var fechaIn = new Date();
                                        var sfechaIn = fechaIn.toISOString().substring(0, 10);
                                        var fechaF = new Date();
                                        var sfechaF = fechaF.toISOString().substring(0, 10);
                                        //console.log(sfechaIn);
                                        $.ajax(
                                            {
                                                url: urlApi + "/proyecto",
                                                type: "POST",
                                                async: true,
                                                data: {
                                                    "fechaInicio":sfechaIn,
                                                    "fechaFin": sfechaF
                                                },
                                                headers: {'Authorization': 'Bearer ' + token},
                                                dataType: "json",
                                                success: function (datos) {
                                                    console.log(datos);
                                                    var idProyecto = datos.id;
                                                    var fechaInicio = new Date();
                                                    var sfechaInicio = fechaInicio.toISOString().substring(0, 10);
                                                    var fechaFin = new Date();
                                                    var sfechaFin = fechaFin.toISOString().substring(0, 10);

                                                    var tr = document.createElement('tr');

                                                    var tdId = document.createElement('td');
                                                    var tdNombre = document.createElement('td');
                                                    var tdFechaInicio = document.createElement('td');
                                                    var tdFechaFin = document.createElement('td');
                                                    var tdCoste = document.createElement('td');
                                                    var tdPresupuesto = document.createElement('td');
                                                    var tdUsuario = document.createElement('td');
                                                    var tdBorrar = document.createElement('td');

                                                    tdNombre.className="nombre";
                                                    tdFechaInicio.className="fechaInicio";
                                                    tdFechaFin.className="fechaFin";
                                                    tdCoste.className="coste";
                                                    tdPresupuesto.className="presupuesto";
                                                    tdUsuario.className="usuario";

                                                    var textoId = document.createTextNode("");
                                                    var textoNombre = document.createTextNode("");
                                                    var inputFechaInicio = document.createElement( "input");
                                                    inputFechaInicio.setAttribute("type", "date");
                                                    inputFechaInicio.setAttribute("id", "fechaInicio"+datos.id);
                                                    inputFechaInicio.setAttribute("value", sfechaInicio);
                                                    var inputFechaFin = document.createElement( "input");
                                                    inputFechaFin.setAttribute("type", "date");
                                                    inputFechaFin.setAttribute("id", "fechaFin"+datos.id);
                                                    inputFechaFin.setAttribute("value",sfechaFin);
                                                    var textoCoste = document.createTextNode(0 );
                                                    var textoPresu = document.createTextNode(0 );
                                                    var textoFotoUser = document.createElement("IMG" );

                                                    textoFotoUser.setAttribute("src", "../../images/interrogacion.jpg");
                                                    textoFotoUser.setAttribute("width", "30");
                                                    textoFotoUser.setAttribute("ondrop","dropProyecto(event)");
                                                    textoFotoUser.setAttribute("ondragover","allowDrop(event)");
                                                    var botonBorrar = document.createElement("input");
                                                    var textoRemove = document.createTextNode("Borrar");
                                                    botonBorrar.setAttribute("id", "borrarBoton_"+datos.id);
                                                    botonBorrar.setAttribute("type", "button");
                                                    botonBorrar.setAttribute("value", "Borrar");

                                                    botonBorrar.appendChild(textoRemove);
                                                    botonBorrar.className="btn btn-danger btn-rounded btn-sm my-0 botonBorrar";
                                                    //console.log(botonBorrar.id);

                                                    tdId.appendChild(textoId);
                                                    tdNombre.appendChild(textoNombre);
                                                    tdFechaInicio.appendChild(inputFechaInicio);
                                                    tdFechaFin.appendChild(inputFechaFin);
                                                    tdCoste.appendChild(textoCoste);
                                                    tdPresupuesto.appendChild(textoPresu);
                                                    tdUsuario.appendChild(textoFotoUser);
                                                    tdBorrar.appendChild(botonBorrar);

                                                    tr.appendChild(tdId);
                                                    tr.appendChild(tdNombre);
                                                    tr.appendChild(tdFechaInicio);
                                                    tr.appendChild(tdFechaFin);
                                                    tr.appendChild(tdCoste);
                                                    tr.appendChild(tdPresupuesto);
                                                    tr.appendChild(tdUsuario);
                                                    tr.appendChild(tdBorrar);
                                                    tblBody.appendChild(tr);

                                                    tr.setAttribute("id", "fila_" + datos.id);
                                                    //console.log(tr.id);
                                                    //console.log(datos.id);
                                                    var nuevoTextoId = document.createTextNode(datos.id);
                                                    tdId.appendChild(nuevoTextoId);

                                                    $(".botonBorrar").click(function () {
                                                        var id = $(this).attr("id");
                                                        //console.log(id);
                                                        //console.log(getIdFromTag(id));
                                                        borrarProyecto(getIdFromTag(id));
                                                    });

                                                    $('#tabla'+this.id).editableTableWidget();
                                                    $('#tabla'+this.id+' td.uneditable').on('change', function (evt, newValue) {
                                                        return false;
                                                    });
                                                    $("#tabla"+this.id+ " tr:last-child td").on('change', function (evt, newValue) {
                                                        // console.log($(this).parent().find(".id").html());
                                                        //console.log(this);
                                                        //console.log(this.nombre);
                                                        var nombre = $(this).parent().find(".nombre").html();
                                                        //console.log(nombre);
                                                        var fechaInicio = document.getElementById("fechaInicio"+datos.id).value;
                                                        //console.log(fechaInicio);
                                                        var fechaFin = document.getElementById("fechaFin"+datos.id).value;
                                                        //console.log(fechaFin);
                                                        var presupuesto = $(this).parent().find(".presupuesto").html();
                                                        //console.log(presupuesto);
                                                        var coste = $(this).parent().find(".coste").html();
                                                        //console.log(coste);

                                                        if (confirm("¿Seguro que desea actualizar tarea?")) {
                                                            $.ajax(
                                                                {
                                                                    url: urlApi+"/proyecto/" + datos.id,
                                                                    type: "PUT",
                                                                    async: true,
                                                                    data: {
                                                                        "coste": coste,
                                                                        "presupuesto": presupuesto,
                                                                        "nombre": nombre,
                                                                        "fechaInicio": fechaInicio,
                                                                        "fechaFin": fechaFin
                                                                    },
                                                                    headers: {'Authorization': 'Bearer ' + token},
                                                                    dataType: "json",
                                                                    success: function (datos) {
                                                                        //console.log(datos);

                                                                    }
                                                                }
                                                            );
                                                        }
                                                    });

                                                }
                                            }
                                        );
                                    }
                                });
                                $(".botonBorrar").click(function () {
                                    var id = $(this).attr("id");
                                    //console.log(id);
                                    //console.log(getIdFromTag(id));
                                    borrarProyecto(getIdFromTag(id));
                                });
                            }

                        });
                    }else if(roles[i]=="ROLE_JEFE"){
                        $("#crearT").show();
                        $("#borrarT").show();
                        $("#crearP").hide();
                        $("#crearUser").hide();
                        $.ajax({
                            url: urlApi+"/proyectos/tareas",
                            type: "GET",
                            async: true,
                            data: {},
                            headers :{'Authorization': 'Bearer '+token},
                            dataType: "json",
                            success: function (proyectos) {
                                //console.log(proyectos);
                                cargarUsuarios();
                                $.each(proyectos, function () {
                                        //console.log("hola");

                                        $('#divTareasProyecto').append('<p style="margin-left: 1vw; font-weight: bold;">'+this.nombre+'</p>');
                                        var idProyecto=this.id;
                                        var tablaProyecto = document.createElement('table');
                                        var tblHead = document.createElement("thead");

                                        var trCabe = document.createElement('tr');
                                        var thIdCabe = document.createElement('th');
                                        var thNombreCabe = document.createElement('th');
                                        var thFechaInicioCabe = document.createElement('th');
                                        var thFechaFinCabe = document.createElement('th');
                                        var thCosteCabe = document.createElement('th');
                                        var thPresupuestoCabe = document.createElement('th');
                                        var thUsuarioCabe = document.createElement('th');
                                        var thRemove = document.createElement('th');

                                        var textoIdCabe = document.createTextNode( 'Id');
                                        var textoNombreCabe = document.createTextNode( 'Nombre');
                                        var textoFechaInicioCabe = document.createTextNode( 'Fecha de inicio');
                                        var textoFechaFinCabe = document.createTextNode( 'Fecha de fin');
                                        var textoCosteCabe = document.createTextNode( 'Coste');
                                        var textoPresuCabe = document.createTextNode( 'Presupuesto');
                                        var textoUsuarioCabe = document.createTextNode( 'Usuario');
                                        var textoRemove = document.createTextNode('Borrar');


                                        thIdCabe.appendChild(textoIdCabe);
                                        thNombreCabe.appendChild(textoNombreCabe);
                                        thFechaInicioCabe.appendChild(textoFechaInicioCabe);
                                        thFechaFinCabe.appendChild(textoFechaFinCabe);
                                        thCosteCabe.appendChild(textoCosteCabe);
                                        thPresupuestoCabe.appendChild(textoPresuCabe);
                                        thUsuarioCabe.appendChild(textoUsuarioCabe);
                                        thRemove.appendChild(textoRemove);


                                        trCabe.appendChild(thIdCabe);
                                        trCabe.appendChild(thNombreCabe);
                                        trCabe.appendChild(thFechaInicioCabe);
                                        trCabe.appendChild(thFechaFinCabe);
                                        trCabe.appendChild(thCosteCabe);
                                        trCabe.appendChild(thPresupuestoCabe);
                                        trCabe.appendChild(thUsuarioCabe);
                                        trCabe.appendChild(thRemove);

                                        tblHead.appendChild(trCabe);
                                        tablaProyecto.appendChild(tblHead);

                                        var tblBody = document.createElement("tbody");
                                        tablaProyecto.appendChild(tblBody);
                                        $.each(this.tareas, function () {
                                            //console.log(this.usuario);

                                            var fechaInicio = new Date(this.fechaInicio.timestamp*1000);
                                            var sfechaInicio = fechaInicio.toISOString().substring(0, 10);

                                            var fechaFin = new Date(this.fechaFin.timestamp*1000);
                                            var sfechaFin = fechaFin.toISOString().substring(0, 10);

                                            var tr = document.createElement('tr');
                                            tr.setAttribute("id", "fila_"+this.id);
                                            var tdId = document.createElement('td');
                                            var tdNombre = document.createElement('td');
                                            var tdFechaInicio = document.createElement('td');
                                            var tdFechaFin = document.createElement('td');
                                            var tdCoste = document.createElement('td');
                                            var tdPresupuesto = document.createElement('td');
                                            var tdUsuario = document.createElement('td');
                                            //tdUsuario.setAttribute("ondrop","drop(event)");
                                            //tdUsuario.setAttribute("ondragover","allowDrop(event)");
                                            var tdRemove = document.createElement('td');

                                            var textoId = document.createTextNode( this.id);
                                            var textoNombre = document.createTextNode( this.nombre);
                                            var inputFechaInicio = document.createElement( "input");
                                            inputFechaInicio.setAttribute("type", "date");
                                            inputFechaInicio.setAttribute("value", sfechaInicio);
                                            var inputFechaFin = document.createElement( "input");
                                            inputFechaFin.setAttribute("type", "date");
                                            inputFechaFin.setAttribute("value",sfechaFin);
                                            var textoCoste = document.createTextNode( this.coste);
                                            var textoPresu = document.createTextNode( this.presupuesto);

                                            var botonRemove = document.createElement("input");
                                            var textoRemove = document.createTextNode("Borrar");
                                            botonRemove.setAttribute("id", "borrarBoton_"+this.id);
                                            botonRemove.setAttribute("type", "button");
                                            botonRemove.setAttribute("value", "Borrar");

                                            botonRemove.appendChild(textoRemove);
                                            botonRemove.className="btn btn-danger btn-rounded btn-sm my-0 botonBorrar";
                                            var textoUsuario = document.createElement("IMG");
                                            if(this.usuario==null){
                                                textoUsuario.setAttribute("src", "../../images/interrogacion.jpg");
                                                textoUsuario.setAttribute("width", "30");
                                                textoUsuario.setAttribute("ondrop", "drop(event)");
                                                textoUsuario.setAttribute("ondragover", "allowDrop(event)");

                                                textoUsuario.setAttribute("height", "30");
                                                textoUsuario.setAttribute("alt", "Foto user");
                                            }else {
                                                textoUsuario.setAttribute("src", this.usuario.fotoUrl);
                                                textoUsuario.setAttribute("width", "30");
                                                textoUsuario.setAttribute("ondrop", "drop(event)");
                                                textoUsuario.setAttribute("ondragover", "allowDrop(event)");

                                                textoUsuario.setAttribute("height", "30");
                                                textoUsuario.setAttribute("alt", "Foto user");
                                            }

                                            tdId.appendChild(textoId);
                                            tdNombre.appendChild(textoNombre);
                                            tdFechaInicio.appendChild(inputFechaInicio);
                                            tdFechaFin.appendChild(inputFechaFin);
                                            tdCoste.appendChild(textoCoste);
                                            tdPresupuesto.appendChild(textoPresu);
                                            tdUsuario.appendChild(textoUsuario);
                                            tdRemove.appendChild(botonRemove);

                                            tr.appendChild(tdId);
                                            tr.appendChild(tdNombre);
                                            tr.appendChild(tdFechaInicio);
                                            tr.appendChild(tdFechaFin);
                                            tr.appendChild(tdCoste);
                                            tr.appendChild(tdPresupuesto);
                                            tr.appendChild(tdUsuario);
                                            tr.appendChild(tdRemove);

                                            tblBody.appendChild(tr);
                                            //console.log(botonRemove);

                                        });
                                        tablaProyecto.appendChild(tblBody);
                                        tablaProyecto.setAttribute("id", "tabla"+this.id);
                                        tablaProyecto.setAttribute("style", "margin-left: 1vw;");

                                        tablaProyecto.className = "pure-table pure-table-bordered table-responsive-md text-center";

                                        document.getElementById('divTareasProyecto').appendChild(tablaProyecto);
                                        $('#divTareasProyecto').append('<button style="margin-top: 1vw" class="w3-button w3-large w3-circle w3-grey" id="añadeTarea'+this.id+'">+</button>');

                                        $("#añadeTarea"+this.id).click(function () {
                                            if (confirm("¿Seguro que desea crear tarea?")) {
                                                // Hacer ajax post Tarea. On succes insertar nueva fila en tabla con id de tarea que devuelve ajax
                                                // con id de tarea en fila
                                                $.ajax(
                                                    {
                                                        url: urlApi + "/tarea",
                                                        type: "POST",
                                                        async: true,
                                                        data: {
                                                            "coste": null,
                                                            "presupuesto": null,
                                                            "nombre": null,
                                                            "fechaInicio": null,
                                                            "fechaFin": null,
                                                            "proyecto": idProyecto
                                                        },
                                                        headers: {'Authorization': 'Bearer ' + token},
                                                        dataType: "json",
                                                        success: function (datos) {
                                                            var fechaInicio = new Date();
                                                            var sfechaInicio = fechaInicio.toISOString().substring(0, 10);
                                                            var fechaFin = new Date();
                                                            var sfechaFin = fechaFin.toISOString().substring(0, 10);

                                                            var tr = document.createElement('tr');

                                                            var tdId = document.createElement('td');
                                                            var tdNombre = document.createElement('td');
                                                            var tdFechaInicio = document.createElement('td');
                                                            var tdFechaFin = document.createElement('td');
                                                            var tdCoste = document.createElement('td');
                                                            var tdPresupuesto = document.createElement('td');
                                                            var tdUsuario = document.createElement('td');
                                                            var tdBorrar = document.createElement('td');

                                                            tdNombre.className="nombre";
                                                            tdFechaInicio.className="fechaInicio";
                                                            tdFechaFin.className="fechaFin";
                                                            tdCoste.className="coste";
                                                            tdPresupuesto.className="presupuesto";
                                                            tdUsuario.className="usuario";

                                                            var textoId = document.createTextNode("");
                                                            var textoNombre = document.createTextNode("");
                                                            var inputFechaInicio = document.createElement( "input");
                                                            inputFechaInicio.setAttribute("type", "date");
                                                            inputFechaInicio.setAttribute("id", "fechaInicio"+idProyecto);
                                                            inputFechaInicio.setAttribute("value", sfechaInicio);
                                                            var inputFechaFin = document.createElement( "input");
                                                            inputFechaFin.setAttribute("type", "date");
                                                            inputFechaFin.setAttribute("id", "fechaFin"+idProyecto);
                                                            inputFechaFin.setAttribute("value",sfechaFin);
                                                            var textoCoste = document.createTextNode(0 );
                                                            var textoPresu = document.createTextNode(0 );
                                                            var textoFotoUser = document.createElement("IMG" );

                                                            textoFotoUser.setAttribute("src", "../../images/interrogacion.jpg");
                                                            textoFotoUser.setAttribute("width", "30");
                                                            textoFotoUser.setAttribute("ondrop","drop(event)");
                                                            textoFotoUser.setAttribute("ondragover","allowDrop(event)");
                                                            var botonBorrar = document.createElement("input");
                                                            var textoRemove = document.createTextNode("Borrar");
                                                            botonBorrar.setAttribute("id", "borrarBoton_"+datos.id);
                                                            botonBorrar.setAttribute("type", "button");
                                                            botonBorrar.setAttribute("value", "Borrar");

                                                            botonBorrar.appendChild(textoRemove);
                                                            botonBorrar.className="btn btn-danger btn-rounded btn-sm my-0 botonBorrar";
                                                            //console.log(botonBorrar.id);

                                                            tdId.appendChild(textoId);
                                                            tdNombre.appendChild(textoNombre);
                                                            tdFechaInicio.appendChild(inputFechaInicio);
                                                            tdFechaFin.appendChild(inputFechaFin);
                                                            tdCoste.appendChild(textoCoste);
                                                            tdPresupuesto.appendChild(textoPresu);
                                                            tdUsuario.appendChild(textoFotoUser);
                                                            tdBorrar.appendChild(botonBorrar);

                                                            tr.appendChild(tdId);
                                                            tr.appendChild(tdNombre);
                                                            tr.appendChild(tdFechaInicio);
                                                            tr.appendChild(tdFechaFin);
                                                            tr.appendChild(tdCoste);
                                                            tr.appendChild(tdPresupuesto);
                                                            tr.appendChild(tdUsuario);
                                                            tr.appendChild(tdBorrar);
                                                            tblBody.appendChild(tr);

                                                            tr.setAttribute("id", "fila_" + datos.id);
                                                            //console.log(tr.id);
                                                            //console.log(datos.id);
                                                            var nuevoTextoId = document.createTextNode(datos.id);
                                                            tdId.appendChild(nuevoTextoId);

                                                            $(".botonBorrar").click(function () {
                                                                var id = $(this).attr("id");
                                                                console.log(id);
                                                                console.log(getIdFromTag(id));
                                                                borrarTarea(getIdFromTag(id));
                                                            });

                                                            $('#tabla'+idProyecto).editableTableWidget();
                                                            $('#tabla'+idProyecto+' td.uneditable').on('change', function (evt, newValue) {
                                                                return false;
                                                            });
                                                            $("#tabla"+idProyecto+ " tr:last-child td").on('change', function (evt, newValue) {
                                                                // console.log($(this).parent().find(".id").html());
                                                                //console.log(this);
                                                                //console.log(this.nombre);
                                                                var nombre = $(this).parent().find(".nombre").html();
                                                                //console.log(nombre);
                                                                var fechaInicio = document.getElementById("fechaInicio"+idProyecto).value;
                                                                //console.log(fechaInicio);
                                                                var fechaFin = document.getElementById("fechaFin"+idProyecto).value;
                                                                //console.log(fechaFin);
                                                                var presupuesto = $(this).parent().find(".presupuesto").html();
                                                                //console.log(presupuesto);
                                                                var coste = $(this).parent().find(".coste").html();
                                                                //console.log(coste);

                                                                if (confirm("¿Seguro que desea actualizar tarea?")) {
                                                                    $.ajax(
                                                                        {
                                                                            url: urlApi+"/tarea/" + datos.id,
                                                                            type: "PUT",
                                                                            async: true,
                                                                            data: {
                                                                                "coste": coste,
                                                                                "presupuesto": presupuesto,
                                                                                "nombre": nombre,
                                                                                "fechaInicio": fechaInicio,
                                                                                "fechaFin": fechaFin
                                                                            },
                                                                            headers: {'Authorization': 'Bearer ' + token},
                                                                            dataType: "json",
                                                                            success: function (datos) {
                                                                                console.log(datos);

                                                                            }
                                                                        }
                                                                    );
                                                                }
                                                            });

                                                        }
                                                    }
                                                );
                                            }
                                        });
                                    });
                                $(".botonBorrar").click(function () {
                                    var id = $(this).attr("id");
                                    console.log(id);
                                    console.log(getIdFromTag(id));
                                    borrarTarea(getIdFromTag(id));
                                });
                            }
                        });
                    }else if(roles[i]=="ROLE_TECNICO"){
                        $("#crearT").hide();
                        $("#borrarT").hide();
                        $("#crearP").hide();
                        $("#crearUser").hide();
                        $("#tablaUsuarios").hide();
                        $("#divProyectos").hide();
                    }
                }
            }
        });
}

function cargarUsuariosRoles() {
    $.ajax(
        {
            url: urlApi + "/usuarios",
            type: "GET",
            async: true,
            data: {},
            headers: {'Authorization': 'Bearer ' + token},
            dataType: "json",
            success: function (usuarios) {
                //console.log(usuarios);
                $.each(usuarios, function () {
                    //console.log(this.roles);
                    if(this.roles.includes("ROLE_JEFE")) {
                        //console.log(this);
                        var tdTecnicos = document.createElement('td');
                        var imagen = document.createElement('IMG');
                        imagen.setAttribute("id", "fotoUsuario_" + this.id);
                        imagen.setAttribute("src", this.fotoUrl);
                        imagen.setAttribute("draggable", "true");
                        imagen.setAttribute("ondragstart", "dragStart(event)");
                        imagen.setAttribute("width", "50");
                        imagen.setAttribute("height", "50");
                        imagen.setAttribute("alt", "Foto user");
                        tdTecnicos.appendChild(imagen);

                        $("#tecnicos").append(tdTecnicos);
                        $("#tusTecnicos").hide();
                    }
                });
            }
        });
}

function cargarUsuarios() {
    $.ajax(
        {
            url: urlApi + "/usuarios",
            type: "GET",
            async: true,
            data: {},
            headers: {'Authorization': 'Bearer ' + token},
            dataType: "json",
            success: function (usuarios) {
                //console.log(usuarios);
                $.each(usuarios, function () {
                    //console.log(usuarios);
                    var tdTecnicos = document.createElement('td');
                    var imagen = document.createElement('IMG');
                    imagen.setAttribute("id", "fotoUsuario_"+this.id);
                    imagen.setAttribute("src", this.fotoUrl);
                    imagen.setAttribute("draggable", "true");
                    imagen.setAttribute("ondragstart", "dragStart(event)");
                    imagen.setAttribute("width", "50");
                    imagen.setAttribute("height", "50");
                    imagen.setAttribute("alt", "Foto user");
                    tdTecnicos.appendChild(imagen);

                    $("#tecnicos").append(tdTecnicos);
                    $("#tusJefes").hide();
                });
            }
        });
}

function borrarTarea(id){
    //console.log("borrar tarea "+id)
    if(confirm("¿Seguro que quiere borrar la tarea "+id+"?")) {
        $.ajax(
            {
                url: urlApi + "/tarea/" + id,
                type: "DELETE",
                async: true,
                data: {},
                headers: {'Authorization': 'Bearer ' + token},
                dataType: "json",

                success: function (tarea) {
                    alert("tarea " + id + " borrada");
                    console.log($("#fila" + id));
                    $("#fila_" + id).fadeOut('slow', function () {
                        $("#fila_" + id).remove();
                        //Deberia hacerse con un if
                        $("#filaTarea" + id).remove();
                    });
                }
            });
    }
}

function borrarProyecto(id){
    //console.log("borrar tarea "+id)
    if(confirm("¿Seguro que quiere borrar el proyecto "+id+"?")) {
        $.ajax(
            {
                url: urlApi + "/proyecto/" + id,
                type: "DELETE",
                async: true,
                data: {},
                headers: {'Authorization': 'Bearer ' + token},
                dataType: "json",

                success: function (tarea) {
                    alert("tarea " + id + " borrada");
                    console.log($("#fila" + id));
                    $("#fila_" + id).fadeOut('slow', function () {
                        $("#fila_" + id).remove();
                        //Deberia hacerse con un if
                    });
                }
            });
    }
}

function dragStart(event) {
    //console.log(event.target.id);
    event.dataTransfer.setData("text", event.target.id);
}

function allowDrop(event) {
    event.preventDefault();
}

function dropProyecto(event) {
    event.preventDefault();
    var data = event.dataTransfer.getData("text");
    //console.log(event.target.parent);
    var textoUsuario = document.createElement("IMG");
    //console.log(document.getElementById(data).src);
    textoUsuario.setAttribute("src", document.getElementById(data).src);
    textoUsuario.setAttribute("width", "30");
    textoUsuario.setAttribute("height", "30");
    textoUsuario.setAttribute("alt", "Foto user");
    textoUsuario.setAttribute("ondrop","dropProyecto(event)");
    textoUsuario.setAttribute("ondragover","allowDrop(event)");
    //console.log(event.target);
    event.target.replaceWith(textoUsuario);
    var idProyecto =getIdFromTag(textoUsuario.parentNode.parentNode.id);
    //event.target.appendChild(document.getElementById(id));


    $.ajax(
        {
            url: urlApi+"/proyecto/"+idProyecto,
            type: "PUT",
            async: true,
            data: {
                "usuario": getIdFromTag(data)
            },
            headers: {'Authorization': 'Bearer ' + token},
            dataType: "json",
            success: function (datos) {
                console.log("Proyecto "+idProyecto+" actualizado");
                cargaTareasUsuario();

            }
        });
}

function drop(event) {
    event.preventDefault();
    var data = event.dataTransfer.getData("text");
    //console.log(event.target.parent);
    var textoUsuario = document.createElement("IMG");
    //console.log(document.getElementById(data).src);
    textoUsuario.setAttribute("src", document.getElementById(data).src);
    textoUsuario.setAttribute("width", "30");
    textoUsuario.setAttribute("height", "30");
    textoUsuario.setAttribute("alt", "Foto user");
    textoUsuario.setAttribute("ondrop","drop(event)");
    textoUsuario.setAttribute("ondragover","allowDrop(event)");
    //console.log(event.target);
    event.target.replaceWith(textoUsuario);
    var idTarea =getIdFromTag(textoUsuario.parentNode.parentNode.id);
    //event.target.appendChild(document.getElementById(id));


    $.ajax(
        {
            url: urlApi+"/tarea/"+idTarea,
            type: "PUT",
            async: true,
            data: {
                "usuario": getIdFromTag(data)
            },
            headers: {'Authorization': 'Bearer ' + token},
            dataType: "json",
            success: function (datos) {
                console.log("tarea "+idTarea+" actualizada");
                cargaTareasUsuario();

            }
        });
}

function getIdFromTag(tag) {
    //Apartir de xxx_18 sacar el 18
    return tag.substring(tag.lastIndexOf("_")+1);

}

function loginUsuario() {
    var div = document.createElement("div");
    div.setAttribute("id", "divLogin");
    var campoUsuario = document.createElement("input");
    campoUsuario.setAttribute("type", "text");
    campoUsuario.setAttribute("id", "idUsuario");
    var password = document.createElement("input");
    password.setAttribute("type", "password");
    password.setAttribute("id", "idPassword");
    var botonLogin = document.createElement("button");
    botonLogin.setAttribute("id", "botonLogin");
    botonLogin.innerHTML="Login";
    var botonReset = document.createElement("button");
    botonReset.setAttribute("id", "reset");
    botonReset.innerHTML="Cancelar";
    var labelUser = document.createElement("label");
    var textoLabel = document.createTextNode("Usuario: ");
    labelUser.appendChild(textoLabel);
    labelUser.setAttribute("for", "idUsuario");
    var labelPass = document.createElement("label");
    var textoLabelPass = document.createTextNode("Contraseña: ");
    labelPass.appendChild(textoLabelPass);
    labelPass.setAttribute("for", "idPassword");

    var br = document.createElement("br");
    var br2 = document.createElement("br");
    var br3 = document.createElement("br");
    var br4 = document.createElement("br");
    var br5 = document.createElement("br");
    var br6 = document.createElement("br");
    var br7 = document.createElement("br");


    div.appendChild(labelUser);
    div.appendChild(br);
    div.appendChild(campoUsuario);
    div.appendChild(br2);
    div.appendChild(labelPass);
    div.appendChild(br3);
    div.appendChild(password);
    div.appendChild(br4);
    div.appendChild(br5);
    div.appendChild(botonLogin);
    div.appendChild(br6);
    div.appendChild(br7);

    var divOriginal = document.getElementById("divDatos");

    divOriginal.replaceWith(div);


    $("#botonLogin").click(function(){
        var usuario= document.getElementById("idUsuario").value;
        var password= document.getElementById("idPassword").value;
        console.log(usuario);
        console.log(password);
        inicia(usuario, password);

    })
}





